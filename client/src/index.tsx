import React from "react";
import ReactDOM from "react-dom";
import "./index.css";
import { UALProvider } from "ual-reactjs-renderer";
import { EOSIOAuth } from "ual-eosio-reference-authenticator";
import { Lynx } from "ual-lynx";
import { TokenPocket } from "ual-token-pocket";
import reportWebVitals from "./reportWebVitals";
import App from "./App";

const chain = {
  chainId: "cf057bbfb72640471fd910bcb67639c22df9f92470936cddc1ade0e2f2e7dc4f",
  rpcEndpoints: [
    {
      protocol: "http",
      host: "api.testnet.eos.io",
      port: "80",
    },
  ],
} as any;

const eosioAuth = new EOSIOAuth([chain], {
  appName: "My App",
  protocol: "eosio",
});
const lynx = new Lynx([chain]);
const tokenPocket = new TokenPocket([chain]);

const supportedAuthenticators = [eosioAuth, lynx, tokenPocket];

ReactDOM.render(
  <UALProvider
    chains={[chain]}
    authenticators={supportedAuthenticators}
    appName="My App"
  >
    <React.StrictMode>
      <App />
    </React.StrictMode>
  </UALProvider>,
  document.getElementById("root")
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
