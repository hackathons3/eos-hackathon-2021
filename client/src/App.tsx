import { AppBar, Typography, Toolbar, Button } from "@material-ui/core";
import React from "react";
import { BrowserRouter, Route, Switch } from "react-router-dom";
import { withUAL } from "ual-reactjs-renderer";
import "./App.css";

function App({ ual: { showModal, hideModal } }: any) {
  return (
    <>
      <AppBar position="static">
        <Toolbar>
          <Typography variant="h6">Some App Name</Typography>
        </Toolbar>
        <Button onClick={showModal}>Login</Button>
        <Button onClick={hideModal}>IDK</Button>
      </AppBar>
      <BrowserRouter>
        <Switch>
          <Route exact path="/">
            <div />
          </Route>
        </Switch>
      </BrowserRouter>
    </>
  );
}

export default withUAL(App);
